/** \class MuNtupleDigiFiller MuNtupleDigiFiller.cc MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleRPCRecHitFiller.cc
 *
 * Helper class : the digi filler for Phase-1 / Phase2 DT digis (the DataFormat is the same)
 *
 * \author C. Battilana (INFN BO)
 *
 *
 */

#include "MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleCSCRecHitFiller.h"

#include "DataFormats/CSCRecHit/interface/CSCRecHit2DCollection.h"
#include "DataFormats/MuonDetId/interface/CSCDetId.h"

#include "FWCore/Framework/interface/LuminosityBlock.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/ESHandle.h"

MuNtupleCSCRecHitFiller::MuNtupleCSCRecHitFiller(edm::ConsumesCollector && collector,
				       const std::shared_ptr<MuNtupleConfig> config,
				       std::shared_ptr<TTree> tree, const std::string & label
				       ) :
  MuNtupleBaseFiller(config, tree, label)
{


  m_cscRecHitToken = collector.consumes<CSCRecHit2DCollection>(m_config->m_inputTags["cscRecHitLabel"]);

}

MuNtupleCSCRecHitFiller::~MuNtupleCSCRecHitFiller()
{

};

void MuNtupleCSCRecHitFiller::initialize()
{

  m_tree->Branch((m_label + "_nRecHit").c_str(), &m_nRecHit, (m_label + "_nRecHit/I").c_str());
  m_tree->Branch((m_label + "_X").c_str(), &m_coordinateX);
  m_tree->Branch((m_label + "_Y").c_str(), &m_coordinateY);
  m_tree->Branch((m_label + "_Z").c_str(), &m_coordinateZ);

  m_tree->Branch((m_label + "_endcap").c_str(), &m_endcap);
  m_tree->Branch((m_label + "_ring").c_str(), &m_ring);
  m_tree->Branch((m_label + "_station").c_str(), &m_station);
  m_tree->Branch((m_label + "_chamber").c_str(), &m_chamber);
  m_tree->Branch((m_label + "_layer").c_str(), &m_layer);


}

void MuNtupleCSCRecHitFiller::clear()
{

  m_nRecHit = 0;

  m_coordinateX.clear();
  m_coordinateY.clear();
  m_coordinateZ.clear();

  m_endcap.clear();
  m_ring.clear();
  m_station.clear();
  m_chamber.clear();
  m_layer.clear();
}

void MuNtupleCSCRecHitFiller::fill(const edm::Event & ev)
{
    clear();

    auto cscRecHits = conditionalGet<CSCRecHit2DCollection>(ev, m_cscRecHitToken, "CSCRecHit2DCollection");
    if(cscRecHits.isValid()){
        for (CSCRecHit2DCollection::const_iterator recHitIt = cscRecHits->begin(); recHitIt != cscRecHits->end(); recHitIt++) {
            m_coordinateX.push_back(recHitIt->localPosition().x());
            m_coordinateY.push_back(recHitIt->localPosition().y());
            m_coordinateZ.push_back(recHitIt->localPosition().z());

						m_nRecHit.push_back(recHitIt->nRecHits());

            CSCDetId cscDetId = (CSCDetId)(*recHitIt).cscDetId();
            m_endcap.push_back(cscDetId.endcap());
            m_ring.push_back(cscDetId.ring());
            m_station.push_back(cscDetId.station());
            m_chamber.push_back(cscDetId.chamber());
            m_layer.push_back(cscDetId.layer());
        }
    }

  return;

}
