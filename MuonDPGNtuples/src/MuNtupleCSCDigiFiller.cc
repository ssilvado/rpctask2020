/** \class MuNtupleDigiFiller MuNtupleDigiFiller.cc MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleDTDigiFiller.cc
 *
 * Helper class : the digi filler for Phase-1 / Phase2 DT digis (the DataFormat is the same)
 *
 * \author C. Battilana (INFN BO)
 *
 *
 */

#include "MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleCSCDigiFiller.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/ESHandle.h"

MuNtupleCSCDigiFiller::MuNtupleCSCDigiFiller(edm::ConsumesCollector && collector,
				       const std::shared_ptr<MuNtupleConfig> config,
				       std::shared_ptr<TTree> tree, const std::string & label,
				       Tag tag) :
  MuNtupleBaseFiller(config, tree, label)
{

  m_cscDigiToken = collector.consumes<CSCDigiCollection>(m_config->m_inputTags["cscDigiTag"]);

}

MuNtupleCSCDigiFiller::~MuNtupleCSCDigiFiller()
{

};

void MuNtupleCSCDigiFiller::initialize()
{

  m_tree->Branch((m_label + "_nDigis").c_str(), &m_nDigis, (m_label + "_nDigis/i").c_str());

  m_tree->Branch((m_label + "_endcap").c_str(),   &m_digi_endcap);
  m_tree->Branch((m_label + "_ring").c_str(),  &m_digi_ring);
  m_tree->Branch((m_label + "_station").c_str(), &m_digi_station);
  m_tree->Branch((m_label + "_chamber").c_str(), &m_digi_chamber);
  m_tree->Branch((m_label + "_layer").c_str(),      &m_digi_layer);

  m_tree->Branch((m_label + "_strip").c_str(), &m_digi_strip);

	m_tree->Branch((m_label + "_wire").c_str(), &m_digi_wire);

	m_tree->Branch((m_label + "_time").c_str(), &m_digi_time);

}

void MuNtupleCSCDigiFiller::clear()
{

  m_nDigis = 0;

  m_digi_endcap.clear();
  m_digi_ring.clear();
  m_digi_station.clear();
  m_digi_chamber.clear();
  m_digi_layer.clear();

  m_digi_strip.clear();

	m_digi_wire.clear();
	m_digi_time.clear();

}

void MuNtupleCSCDigiFiller::fill(const edm::Event & ev)
{

  clear();

  auto cscstripDigis = conditionalGet<CSCStripDigiCollection>(ev, m_cscstripDigiToken,"CSCStripDigiCollection");

  if (cscstripDigis.isValid()){

      auto cscstripLayerIdIt  = cscstripDigis->begin();
      auto cscstripLayerIdEnd = cscstripDigis->end();

      for (; cscstripLayerIdIt != cscstripLayerIdEnd; ++cscstripLayerIdIt){

	  		const auto & cscstripLayerId = (*cscstripLayerIdIt).first;

	  		auto digiIt  = (*cscstripLayerIdIt).second.first;
	  		auto digiEnd = (*cscstripLayerIdIt).second.second;

	  		for (; digiIt != digiEnd; ++digiIt){
	      	m_digi_endcap.push_back(cscstripLayerId.endcap());
	      	m_digi_station.push_back(cscstripLayerId.station());
	      	m_digi_ring.push_back(cscstripLayerId.ring());
	      	m_digi_chamber.push_back(cscstripLayerId.chamberr());
	      	m_digi_layer.push_back(cscstripLayerId.layer());

	      	m_digi_strip.push_back(digiIt->getStrip());

	      	m_nDigis++;
	      }
	     }
  }

  auto cscwireDigis = conditionalGet<CSCWireDigiCollection>(ev, m_cscwireDigiToken,"CSCWireDigiCollection");

  if (cscwireDigis.isValid()){

      auto cscwireLayerIdIt  = cscwireDigis->begin();
      auto cscwireLayerIdEnd = cscwireDigis->end();

      for (; cscwireLayerIdIt != cscwireLayerIdEnd; ++cscwireLayerIdIt){

	  		const auto & cscwireLayerId = (*cscwireLayerIdIt).first;

	  		auto digiIt  = (*cscwireLayerIdIt).second.first;
	  		auto digiEnd = (*cscwireLayerIdIt).second.second;

	  		for (; digiIt != digiEnd; ++digiIt){
	      	m_digi_wire.push_back(digiIt->getWireGroup());
					m_digi_time.push_back(digiIt->getTimeBin());

	      }
	     }
  }

  return;

}
