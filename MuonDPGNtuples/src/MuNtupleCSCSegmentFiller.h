#ifndef MuNtuple_MuNtupleCSCSegmentFiller_h
#define MuNtuple_MuNtupleCSCSegmentFiller_h

/** \class MuNtupleDTSegmentFiller MuNtupleDTSegmentFiller.h MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleDTSegmentFiller.h
 *
 * Helper class : the segment filler for Phase-1 / Phase2 DT segments (the DataFormat is the same)
 *
 * \author C. Battilana (INFN BO)
 *
 *
 */

#include "MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleBaseFiller.h"
#include "DataFormats/DTRecHit/interface/CSCSegmentCollection.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include <vector>

class MuNtupleCSCSegmentFiller : public MuNtupleBaseFiller
{

 public:

  /// Constructor
  MuNtupleCSCSegmentFiller(edm::ConsumesCollector && collector,
		     const std::shared_ptr<MuNtupleConfig> config,
		     std::shared_ptr<TTree> tree, const std::string & label);

  ///Destructor
  virtual ~MuNtupleCSCSegmentFiller();

  /// Intialize function : setup tree branches etc ...
  virtual void initialize() final;

  /// Clear branches before event filling
  virtual void clear() final;

  /// Fill tree branches for a given event
  virtual void fill(const edm::Event & ev) final;

 private :

  /// The digi token
  edm::EDGetTokenT<CSCSegmentCollection> m_cscSegmentToken;

  /// The variables holding
  /// all digi related information

  unsigned int m_nSegments; // the # of digis (size of all following vectors)

  std::vector<short> m_seg_endcap;   // wheel (short in [-2:2] range)
  std::vector<short> m_seg_ring;  // sector (short in [1:14] range)
                                      // sector 13 used for the second MB4 of sector 4
                                      // sector 14 used for the second MB4 of sector 10
  std::vector<short> m_seg_station; // station (short in [1:4] range)

  std::vector<short> m_seg_chamber; // has segment phi view (0/1 = no/yes)
  std::vector<short> m_seg_layer; // has segment zed view (0/1 = no/yes)

  std::vector<float> m_seg_posLoc_x; // position x in local coordinates (float in cm)
  std::vector<float> m_seg_posLoc_y; // position y in local coordinates (float in cm)
  std::vector<float> m_seg_posLoc_z; // position z in local coordinates (float in cm)
  std::vector<float> m_seg_dirLoc_x; // direction x in local coordinates (float)
  std::vector<float> m_seg_dirLoc_y; // direction y in local coordinates (float)
  std::vector<float> m_seg_dirLoc_z; // direction z in local coordinates (float)
};

#endif
