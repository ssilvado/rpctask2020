/** \class MuNtupleDTSegmentFiller MuNtupleDTSegmentFiller.cc DTDPGAnalysis/MuonDPGNtuples/src/MuNtupleDTSegmentFiller.cc
 *
 * Helper class : the segment filler for Phase-1 / Phase2 segments (the DataFormat is the same)
 *
 * \author C. Battilana (INFN BO)
 *
 *
 */

#include "MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleCSCSegmentFiller.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/ESHandle.h"


MuNtupleCSCSegmentFiller::MuNtupleCSCSegmentFiller(edm::ConsumesCollector && collector,
				       const std::shared_ptr<MuNtupleConfig> config,
				       std::shared_ptr<TTree> tree, const std::string & label,
				       Tag tag) :
  MuNtupleBaseFiller(config, tree, label)
{

  m_cscSegmentToken = collector.consumes<CSCSegmentCollection>(m_config->m_inputTags["cscSegmentTag"]);

}

MuNtupleCSCSegmentFiller::~MuNtupleCSCSegmentFiller()
{

};

void MuNtupleCSCSegmentFiller::initialize()
{

  m_tree->Branch((m_label + "_nSegments").c_str(), &m_nSegments, (m_label + "_nSegments/i").c_str());

  m_tree->Branch((m_label + "_endcap").c_str(),   &m_seg_endcap);
  m_tree->Branch((m_label + "_ring").c_str(),  &m_seg_ring);
  m_tree->Branch((m_label + "_station").c_str(), &m_seg_station);
  m_tree->Branch((m_label + "_chamber").c_str(), &m_seg_chamber);
  m_tree->Branch((m_label + "_layer").c_str(), &m_seg_layer);

  m_tree->Branch((m_label + "_posLoc_x").c_str(), &m_seg_posLoc_x);
  m_tree->Branch((m_label + "_posLoc_y").c_str(), &m_seg_posLoc_y);
  m_tree->Branch((m_label + "_posLoc_z").c_str(), &m_seg_posLoc_z);
  m_tree->Branch((m_label + "_dirLoc_x").c_str(), &m_seg_dirLoc_x);
  m_tree->Branch((m_label + "_dirLoc_y").c_str(), &m_seg_dirLoc_y);
  m_tree->Branch((m_label + "_dirLoc_z").c_str(), &m_seg_dirLoc_z);

}

void MuNtupleCSCSegmentFiller::clear()
{

  m_nSegments = 0;

  m_seg_endcap.clear();
  m_seg_ring.clear();
  m_seg_station.clear();
  m_seg_chamber.clear();
  m_seg_layer.clear();

  m_seg_posLoc_x.clear();
  m_seg_posLoc_y.clear();
  m_seg_posLoc_z.clear();
  m_seg_dirLoc_x.clear();
  m_seg_dirLoc_y.clear();
  m_seg_dirLoc_z.clear();

}

void MuNtupleCSCSegmentFiller::fill(const edm::Event & ev)
{

  clear();

  auto segments = conditionalGet<CSCSegmentCollection>(ev, m_cscSegmentToken,"CSCSegmentCollection");

  if (segments.isValid()){

      auto chambIt  = segments->id_begin();
      auto chambEnd = segments->id_end();

      for (; chambIt != chambEnd; ++chambIt){

	  		const auto range = segments4D->get(*chambIt);

	  		auto segment    = range.first;  // CB check naming
	  		auto segmentEnd = range.second;

	  		for (; segment != segmentEnd; ++segment){

	      	auto endcap   = (*chambIt).endcap();
	      	auto ring  = (*chambIt).ring();
	      	auto station = (*chambIt).station();
					auto chamber   = (*chambIt).chamber();
	      	auto layer  = (*chambIt).layer();

	      	m_seg_endcap.push_back(endcap);
	      	m_seg_ring.push_back(ring);
	      	m_seg_station.push_back(station);
					m_seg_chamber.push_back(chamber);
	      	m_seg_layer.push_back(layer);

	      	auto pos = segment->localPosition();
	      	auto dir = segment->localDirection();

	      	m_seg_posLoc_x.push_back(pos.x());
	      	m_seg_posLoc_y.push_back(pos.y());
	      	m_seg_posLoc_z.push_back(pos.z());

	      	m_seg_dirLoc_x.push_back(dir.x());
	      	m_seg_dirLoc_y.push_back(dir.y());
	      	m_seg_dirLoc_z.push_back(dir.z());

	        ++m_nSegments;
	      }
	    }

    }

  return;

}
