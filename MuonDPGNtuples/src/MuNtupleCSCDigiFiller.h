#ifndef MuNtuple_MuNtupleCSCDigiFiller_h
#define MuNtuple_MuNtupleCSCDigiFiller_h

/** \class MuNtupleDTDigiFiller MuNtupleDTDigiFiller.h MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleDTDigiFiller.h
 *
 * Helper class : the digi filler for Phase-1 / Phase2 DT digis (the DataFormat is the same)
 *
 * \author C. Battilana (INFN BO)
 *
 *
 */

#include "MuDPGAnalysis/MuonDPGNtuples/src/MuNtupleBaseFiller.h"
#include "DataFormats/CSCDigi/interface/CSCWireDigiCollection.h"
#include "DataFormats/CSCDigi/interface/CSCStripDigiCollection.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"

#include <vector>

class MuNtupleCSCDigiFiller : public MuNtupleBaseFiller
{

 public:

  /// Constructor
  MuNtupleCSCDigiFiller(edm::ConsumesCollector && collector,
		     const std::shared_ptr<MuNtupleConfig> config,
		     std::shared_ptr<TTree> tree, const std::string & label);

  ///Destructor
  virtual ~MuNtupleCSCDigiFiller();

  /// Intialize function : setup tree branches etc ...
  virtual void initialize() final;

  /// Clear branches before event filling
  virtual void clear() final;

  /// Fill tree branches for a given events
  virtual void fill(const edm::Event & ev) final;

 private :

  /// The digi token
  edm::EDGetTokenT<CSCRPCDigiCollection> m_cscDigiToken;

  /// The variables holding
  /// all digi related information

  unsigned int m_nDigis; // the # of digis (size of all following vectors)

  std::vector<short> m_digi_endcap;   // wheel (short in [-2:2] range)
  std::vector<short> m_digi_ring;  // sector (short in [1:14] range)
                                     // sector 13 used for the second MB4 of sector 4
                                     // sector 14 used for the second MB4 of sector 10
  std::vector<short> m_digi_station; // station (short in [1:4] range)

  std::vector<short> m_digi_chamber; // superlayer (short in [1:3] range)
                                        // SL 1 and 3 are phi SLs
                                        // SL 2 is theta SL
  std::vector<short> m_digi_layer;      // layer (short in [1:4] range)
  std::vector<short> m_digi_strip;       // wire (short in [1:X] range)
                                        // X varies for different chambers SLs and layers

  std::vector<float> m_digi_wire;

  std::vector<float> m_digi_time;

};

#endif
